using MassTransit;
using Microsoft.AspNetCore.Components.Web.Virtualization;
using Microsoft.AspNetCore.Mvc;
using Play.Catalog.Contracts;
using Play.Catalog.Service.Dtos;
using Play.Common;

namespace Play.Catalog.Service.Controllers
{
    [ApiController]
    [Route("items")]
    public class ItemsController:ControllerBase
    {
        private readonly IRepository<Item> _repo;
        private readonly IPublishEndpoint _publishEndPoint;

        public ItemsController(IRepository<Item> repo, IPublishEndpoint publishEndpoint)
        {
            _repo = repo;
            _publishEndPoint = publishEndpoint;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ItemDto>>> GetAsync()
        {
            
            var items =  (await _repo.GetAllAsync())
            .Select(item => item.AsDto());
            return Ok(items);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ItemDto>> GetByIdAsync(Guid id)
        {
            var item = await _repo.GetAsync(id);
            if (item == null)
            {
                return NotFound();
            }
            return item.AsDto();

        }
        [HttpPost]
        public async Task<ActionResult<ItemDto>> PostAsync(CreateItemDto dto)
        {
            var item = new Item
            {
                Name = dto.Name, 
                Description = dto.Description, 
                Price = dto.Price, 
                CreatedAt = DateTimeOffset.UtcNow
            };
            await _repo.CreateAsync(item);
            await _publishEndPoint.Publish(new CatalogItemCreated(item.Id, item.Name, item.Description));
            return CreatedAtAction(nameof(GetByIdAsync), new{id = item.Id}, item);
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(Guid id, UpdateItemDto dto) 
        {
            var existingItem = await _repo.GetAsync(id);
            if (existingItem == null)
            {
                return NotFound();
            }
            existingItem.Name = dto.Name;
            existingItem.Description = dto.Description;
            existingItem.Price = dto.Price;
            
            await _repo.UpdateAsync(existingItem);

            await _publishEndPoint.Publish(new CatalogItemUpdated(existingItem.Id, existingItem.Name, existingItem.Description));
            return NoContent();
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            var item = await _repo.GetAsync(id);
            if (item == null)
            {
                return NotFound();
            }
            await _repo.RemoveAsync(item.Id); 

            await _publishEndPoint.Publish(new CatalogItemDeleted(id));
            return NoContent();
        }
    }
}