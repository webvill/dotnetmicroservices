dotnet add package MongoDb.Driver
dotnet add package Microsoft.Extensions.Configuration
dotnet add package Microsoft.Extensions.Configuration.Binder
dotnet add package Microsoft.Extensions.DependencyInjection
 
dotnet pack -o ..\..\..\packages\
dotnet pack -p:PackageVersion=1.0.1 -o C:projects\packages\