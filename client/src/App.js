import { BrowserRouter, Routes, Route } from "react-router-dom";
import Layout from "./components/Layout";
import Home from "./components/Home";
import Catalog from "./components/Catalog";
import Inventory from "./components/Inventory";
import "./App.css";

function App() {
   return (
      <BrowserRouter>
         <Routes>
            <Route path="/" element={<Layout />}>
               <Route index element={<Home />} />
               <Route path="catalog" element={<Catalog />} />
               <Route path="inventory" element={<Inventory />} />
            </Route>
         </Routes>
      </BrowserRouter>
   );
}

export default App;
