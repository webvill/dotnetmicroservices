import { useState, useEffect } from "react";
import { Col, Container, Row, Table, Button, Form } from "react-bootstrap";

const Inventory = () => {
   const [state, setState] = useState({
      userId: "",
      items: [],
      renderItems: false,
      loading: true,
      loadedSuccess: false,
   });
   const onChange = (e) => {
      setState({ ...state, [e.target.name]: e.target.value });
   };
   /* useEffect(() => {
      populateItems();
   }, []); */

   const populateItems = async () => {
      if (state.userId === "") {
         return;
      }

      setState({
         ...state,
         items: [],
         renderItems: true,
         loading: true,
         loadedSuccess: false,
      });
      console.log(state.renderItems);
      fetch(`https://localhost:5002/items?userId=${state.userId}`)
         .then((response) => response.json())
         .then((returnedItems) =>
            setState({
               ...state,
               items: returnedItems,
               loading: false,
               loadedSuccess: true,
            })
         )
         .catch((err) => {
            console.log(err);
            setState({
               ...state,
               items: [],
               loading: false,
               loadedSuccess: false,
            });
         });
   };
   function renderInputs() {
      return (
         <Form>
            <Form.Label htmlFor="userId">User Id:</Form.Label>
            <Form.Control
               className="mb-2 me-sm-2"
               style={{ minWidth: "350px" }}
               type="text"
               name="userId"
               id="userId"
               placeholder="Enter a user id"
               onChange={onChange}
               value={state.userId}
            />
            <Button
               className="mb-2"
               variant="primary"
               onClick={() => populateItems()}
            >
               Get Inventory
            </Button>
         </Form>
      );
   }
   function renderItemsTable() {
      return state.loading ? (
         <p>
            <em>Loading...</em>
         </p>
      ) : state.loadedSuccess === false ? (
         <p>Could not load items</p>
      ) : (
         <Container style={{ paddingTop: "10px", paddingLeft: "0px" }}>
            <Row>
               <Col>
                  <Table striped>
                     <thead className="thead-dark">
                        <tr>
                           <th>Name</th>
                           <th>Description</th>
                           <th>Quantity</th>
                        </tr>
                     </thead>
                     <tbody>
                        {!state.items || state.items.length <= 0 ? (
                           <tr>
                              <td colSpan="6" align="center">
                                 <b>No Items yet</b>
                              </td>
                           </tr>
                        ) : (
                           state.items.map((item) => (
                              <tr key={item.catalogItemId}>
                                 <td>{item.name}</td>
                                 <td>{item.description}</td>
                                 <td>{item.quantity}</td>
                              </tr>
                           ))
                        )}
                     </tbody>
                  </Table>
               </Col>
            </Row>
         </Container>
      );
   }

   return (
      <div>
         <h1 id="tabelLabel">Inventory</h1>
         {renderInputs()}
         {renderItemsTable()}
      </div>
   );
};

export default Inventory;
