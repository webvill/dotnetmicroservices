import React from "react";
import { Container, Navbar, Nav } from "react-bootstrap";
import { Link } from "react-router-dom";
const NavMenu = () => {
   return (
      <header>
         <Navbar bg="light" expand="lg">
            <Container>
               <Navbar.Brand as={Link} to="/">
                  Play Economy
               </Navbar.Brand>
               <Navbar.Toggle aria-controls="basic-navbar-nav" />
               <Navbar.Collapse id="basic-navbar-nav">
                  <Nav className="mr-auto">
                     <Nav.Link as={Link} to="/">
                        Home
                     </Nav.Link>
                     <Nav.Link as={Link} to="/catalog">
                        Catalog
                     </Nav.Link>
                     <Nav.Link as={Link} to="/inventory">
                        Inventory
                     </Nav.Link>
                  </Nav>
               </Navbar.Collapse>
            </Container>
         </Navbar>
      </header>
   );
};

export default NavMenu;
