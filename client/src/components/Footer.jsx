import React from "react";

const Footer = () => {
   return (
      <footer className="footer border-top text-muted">
         <div className="container">&copy; 2021 - Play Economy</div>
      </footer>
   );
};

export default Footer;
