import { Col, Container, Row, Table, Button } from "react-bootstrap";
import ItemModal from "./form/ItemModal";
import GrantItemModal from "./form/GrantItemModal";
import { useState, useEffect } from "react";

const Catalog = () => {
   const [state, setState] = useState({
      items: [],
      loading: true,
      loadedSuccess: false,
   });
   useEffect(() => {
      populateItems();
   }, []);

   const populateItems = async () => {
      fetch("https://localhost:5000/items")
         .then((response) => response.json())
         .then((returnedItems) =>
            setState({
               ...state,
               items: returnedItems,
               loading: false,
               loadedSuccess: true,
            })
         )
         .catch((err) => {
            console.log(err);
            setState({ items: [], loading: false, loadedSuccess: false });
         });
   };
   const addItemToState = (item) => {
      setState({
         ...state,
         items: [...state.items, item],
      });
   };

   const updateState = (id) => {
      populateItems();
   };
   const deleteItemFromState = (id) => {
      const updated = state.items.filter((item) => item.id !== id);
      setState({ ...state, items: updated });
   };
   const deleteItem = async (id) => {
      let confirmDeletion = window.confirm("Do you really wish to delete it?");
      if (confirmDeletion) {
         fetch(`https:/localhost:5000/items/${id}`, {
            method: "delete",
            headers: {
               "Content-Type": "application/json",
            },
         })
            .then((res) => {
               deleteItemFromState(id);
            })
            .catch((err) => {
               console.log(err);
               window.alert("Could not delete the item.");
            });
      }
   };
   function renderItemsTable(items) {
      return (
         <Container style={{ paddingTop: "10px", paddingLeft: "0px" }}>
            <Row>
               <Col>
                  <Table striped bordered hover>
                     <thead className="thead-dark">
                        <tr>
                           <th>Name</th>
                           <th>Description</th>
                           <th>Price</th>
                           <th style={{ textAlign: "center" }}>Actions</th>
                        </tr>
                     </thead>
                     <tbody>
                        {!items || items.length <= 0 ? (
                           <tr>
                              <td colSpan="6" align="center">
                                 <b>No Items yet</b>
                              </td>
                           </tr>
                        ) : (
                           items.map((item) => (
                              <tr key={item.id}>
                                 <td>{item.name}</td>
                                 <td>{item.description}</td>
                                 <td>{item.price}</td>
                                 <td align="center">
                                    <div>
                                       <ItemModal
                                          isNew={false}
                                          item={item}
                                          updateItemIntoState={updateState}
                                       />
                                       &nbsp;&nbsp;&nbsp;
                                       <GrantItemModal item={item} />
                                       &nbsp;&nbsp;&nbsp;
                                       <Button
                                          variant="danger"
                                          onClick={() => deleteItem(item.id)}
                                       >
                                          Delete
                                       </Button>
                                    </div>
                                 </td>
                              </tr>
                           ))
                        )}
                     </tbody>
                  </Table>
               </Col>
            </Row>
            <Row>
               <Col>
                  <ItemModal isNew={true} addItemToState={addItemToState} />
               </Col>
            </Row>
         </Container>
      );
   }

   return (
      <div>
         <h1 id="tabelLabel">Catalog Items</h1>
         {state.loading ? (
            <p>
               <em>Loading...</em>
            </p>
         ) : state.loadedSuccess ? (
            renderItemsTable(state.items)
         ) : (
            <p>Could not load items</p>
         )}
      </div>
   );
};

export default Catalog;
