import { Outlet } from "react-router-dom";
import NavMenu from "./NavMenu";
import Footer from "./Footer";
import { Container } from "react-bootstrap";
const Layout = () => {
   return (
      <div>
         <NavMenu />
         <Container>
            <Outlet />
         </Container>
         <Footer />
      </div>
   );
};

export default Layout;
