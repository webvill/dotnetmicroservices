import React, { useState } from "react";
import { Button, Modal } from "react-bootstrap";
import GrantItemForm from "./GrantItemForm";
const GrantItemModal = (props) => {
   const [modal, setModal] = useState(false);
   const toggle = () => {
      setModal((previous) => !previous);
   };
   return (
      <>
         <Button variant="primary" onClick={toggle}>
            Grant
         </Button>
         <Modal show={modal} className={props.className} onHide={toggle}>
            <Modal.Header closeButton>Grant {props.item.name}</Modal.Header>
            <Modal.Body>
               <GrantItemForm toggle={toggle} item={props.item} />
            </Modal.Body>
         </Modal>
      </>
   );
};

export default GrantItemModal;
