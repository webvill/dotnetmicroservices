import { useState } from "react";
import { Button, Modal } from "react-bootstrap";
import ItemForm from "./ItemForm";

const ItemModal = (props) => {
   const [modal, setModal] = useState(false);
   const toggle = () => {
      setModal((previous) => !previous);
   };
   const isNew = props.isNew;
   let title = "Edit Item";
   let button = "";
   if (isNew) {
      title = "Add Item";
      button = (
         <Button
            variant="primary"
            onClick={toggle}
            style={{ minWidth: "200px" }}
         >
            Add
         </Button>
      );
   } else {
      button = (
         <Button variant="primary" onClick={toggle}>
            Edit
         </Button>
      );
   }

   return (
      <>
         {button}
         <Modal show={modal} className={props.className} onHide={toggle}>
            <Modal.Header closeButton>{title}</Modal.Header>
            <Modal.Body>
               <ItemForm
                  addItemToState={props.addItemToState}
                  updateItemIntoState={props.updateItemIntoState}
                  toggle={toggle}
                  item={props.item}
               />
            </Modal.Body>
         </Modal>
      </>
   );
};

export default ItemModal;
