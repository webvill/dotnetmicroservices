import React, { useEffect, useState } from "react";
import { Button, Form, Alert } from "react-bootstrap";

const ItemForm = (props) => {
   const [state, setState] = useState({
      id: 0,
      name: "",
      description: "",
      price: "",
      alertVisible: false,
      validated: false,
   });

   useEffect(() => {
      if (props.item) {
         const { id, name, description, price } = props.item;
         setState({ id, name, description, price });
      }
   }, []);
   const onChange = (e) => {
      setState({ ...state, [e.target.name]: e.target.value });
      console.log("state", state);
   };
   const createItem = async () => {
      console.log("state", state);
      fetch("https://localhost:5000/items", {
         method: "post",
         headers: {
            "Content-Type": "application/json",
         },
         body: JSON.stringify({
            name: state.name,
            description: state.description,
            price: parseFloat(state.price),
         }),
      })
         .then(async (response) => {
            if (!response.ok) {
               const errorData = await response.json();
               console.error(errorData);
               throw new Error(`Could not add the item: ${errorData.title}`);
            }

            return response.json();
         })
         .then((item) => {
            props.addItemToState(item);
            props.toggle();
         })
         .catch((err) => {
            showAlert(err.message);
         });
   };

   const submitEdit = (e) => {
      e.preventDefault();

      const form = e.currentTarget;
      if (form.checkValidity() === false) {
         e.stopPropagation();
      } else {
         updateItem();
      }

      setState({ ...state, validated: true });
   };
   const submitNew = (e) => {
      e.preventDefault();

      const form = e.currentTarget;
      if (form.checkValidity() === false) {
         e.stopPropagation();
      } else {
         createItem();
      }

      setState({ ...state, validated: true });
   };
   const updateItem = async () => {
      fetch(`https://localhost:5000/items/${state.id}`, {
         method: "put",
         headers: {
            "Content-Type": "application/json",
         },
         body: JSON.stringify({
            id: state.id,
            name: state.name,
            description: state.description,
            price: parseFloat(state.price),
         }),
      })
         .then(async (response) => {
            if (!response.ok) {
               const errorData = await response.json();
               console.error(errorData);
               throw new Error(`Could not update the item: ${errorData.title}`);
            }

            props.toggle();
            props.updateItemIntoState(state.id);
         })
         .catch((err) => {
            showAlert(err.message);
         });
   };

   const showAlert = (message) => {
      setState({
         ...state,
         alertMessage: message,
         alertColor: "danger",
         alertVisible: true,
      });
   };

   return (
      <Form
         noValidate
         validated={state.validated}
         onSubmit={props.item ? submitEdit : submitNew}
      >
         <Form.Group>
            <Form.Label htmlFor="name">Name:</Form.Label>
            <Form.Control
               type="text"
               name="name"
               onChange={onChange}
               value={state.name}
               required
            />
            <Form.Control.Feedback type="invalid">
               The Name field is required
            </Form.Control.Feedback>
         </Form.Group>
         <Form.Group>
            <Form.Label htmlFor="description">Description:</Form.Label>
            <Form.Control
               type="text"
               name="description"
               onChange={onChange}
               value={state.description}
            />
         </Form.Group>
         <Form.Group>
            <Form.Label htmlFor="price">Price:</Form.Label>
            <Form.Control
               type="number"
               name="price"
               onChange={onChange}
               value={state.price}
               required
            />
            <Form.Control.Feedback type="invalid">
               The Price field is required
            </Form.Control.Feedback>
         </Form.Group>
         <Button variant="primary" type="submit">
            Save
         </Button>

         <Alert
            style={{ marginTop: "10px" }}
            variant={state.alertColor}
            show={state.alertVisible}
         >
            {state.alertMessage}
         </Alert>
      </Form>
   );
};

export default ItemForm;
