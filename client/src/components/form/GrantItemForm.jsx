import React, { useState, useEffect } from "react";
import { Form, Button, Alert } from "react-bootstrap";
import { v4 as uuidv4 } from "uuid";

const GrantItemForm = (props) => {
   const [state, setState] = useState({
      validate: false,
      userId: "",
      quantity: 0,
      alertColor: "red",
      alertVisible: true,
      alertMessage: "",
   });

   useEffect(() => {
      const { id } = props.item;
      setState({ ...state, id });
   }, []);

   const onChange = (e) => {
      setState({ ...state, [e.target.name]: e.target.value });
   };

   const submitGrant = (e) => {
      e.preventDefault();

      const form = e.currentTarget;
      if (form.checkValidity() === false) {
         e.stopPropagation();
      } else {
         grantItem();
      }

      setState({ ...state, validated: true });
   };

   const grantItem = async () => {
      fetch("https://localhost:5002/items", {
         method: "post",
         headers: {
            "Content-Type": "application/json",
         },
         body: JSON.stringify({
            userId: state.userId,
            catalogItemId: state.id,
            quantity: parseInt(state.quantity),
         }),
      })
         .then(async (response) => {
            if (!response.ok) {
               const errorData = await response.json();
               console.error(errorData);
               throw new Error(`Could not grant the item: ${errorData.title}`);
            }

            props.toggle();
         })
         .catch((err) => {
            showAlert(err.message);
         });
   };

   const showAlert = (message) => {
      this.setState({
         ...state,
         alertMessage: message,
         alertColor: "danger",
         alertVisible: true,
      });
   };

   return (
      <div>
         <Form noValidate validated={state.validated} onSubmit={submitGrant}>
            <Form.Group>
               <Form.Label htmlFor="userId">User Id:</Form.Label>
               <Form.Control
                  type="text"
                  name="userId"
                  onChange={onChange}
                  value={state.userId}
                  required
               />
               <Form.Control.Feedback type="invalid">
                  The User Id field is required
               </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
               <Form.Label htmlFor="quantity">Quantity:</Form.Label>
               <Form.Control
                  type="number"
                  name="quantity"
                  onChange={onChange}
                  value={state.quantity}
                  required
               />
               <Form.Control.Feedback type="invalid">
                  The Quantity field is required
               </Form.Control.Feedback>
            </Form.Group>
            <Button variant="primary" type="submit">
               Grant
            </Button>

            <Alert
               style={{ marginTop: "10px" }}
               variant={state.alertColor}
               show={state.alertVisible}
            >
               {state.alertMessage}
            </Alert>
         </Form>
      </div>
   );
};

export default GrantItemForm;
