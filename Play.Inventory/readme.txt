dotnet new webapi -n Play.Inventory.Service
from src/Play.Inventory.Service => dotnet add package Play.Common

dotnet add package Microsoft.Extensions.Http.Polly
dotnet add package Play.Catalog.Contracts