using MassTransit;
using Play.Catalog.Contracts;
using Play.Common;
using Play.Inventory.Service.Entities;

namespace Play.Inventory.Service.Consumers
{
    public class CatalogItemDeletedConsumer : IConsumer<CatalogItemDeleted>
    {
        private readonly IRepository<CatalogItem> _repo;
        public CatalogItemDeletedConsumer(IRepository<CatalogItem> repo)
        {
            _repo = repo;
        }
        public async Task Consume(ConsumeContext<CatalogItemDeleted> context)
        {
            var message = context.Message;
            var item = await _repo.GetAsync(message.ItemId);
            if (item == null)
            {
                return;
            }
            
            await _repo.RemoveAsync(message.ItemId);
        }
    }
}