using MassTransit;
using Play.Catalog.Contracts;
using Play.Common;
using Play.Inventory.Service.Entities;

namespace Play.Inventory.Service.Consumers
{
    public class CatalogItemCreatedConsumer : IConsumer<CatalogItemCreated>
    {
        private readonly IRepository<CatalogItem> _repo;
        public CatalogItemCreatedConsumer(IRepository<CatalogItem> repo)
        {
            _repo = repo;
        }
        public async Task Consume(ConsumeContext<CatalogItemCreated> context)
        {
            var message = context.Message;
            var item = await _repo.GetAsync(message.ItemId);
            // if the publisher publish the same message twice
            if (item != null)
            {
                return;
            }
            item = new CatalogItem
            {
                Id = message.ItemId,
                Name = message.Name,
                Description = message.Description
            };
            await _repo.CreateAsync(item);
        }
    }
}