using Microsoft.AspNetCore.Mvc;
using Play.Common;
using Play.Inventory.Service.Clients;
using Play.Inventory.Service.Dtos;
using Play.Inventory.Service.Entities;
namespace Play.Inventory.Service.Controllers
{
    [ApiController]
    [Route("items")]
    public class ItemsController : ControllerBase 
    {
        private readonly IRepository<InventoryItem> _inventoryItemsRepo;
        /* private readonly CatalogClient _catalogClient; */
        private readonly IRepository<CatalogItem> _catalogItemsRepo;
        public ItemsController(
            /* CatalogClient client */
            IRepository<InventoryItem> inventoryItemRepo, 
            IRepository<CatalogItem> catalogItemsRepo
            )
        {
            /* _catalogClient = client; */
            _inventoryItemsRepo = inventoryItemRepo;
            _catalogItemsRepo = catalogItemsRepo;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<InventoryItemDto>>> GetAsync(Guid userId)
        {
            if (userId == Guid.Empty)
            {
                return BadRequest();
            }
            var inventoryItemEntities = await _inventoryItemsRepo.GetAllAsync(item => item.UserId == userId);
            var itemIds = inventoryItemEntities.Select(item => item.CatalogItemId);
            var catalogItemEntities = await _catalogItemsRepo.GetAllAsync(item => itemIds.Contains(item.Id));

            var inventoryItemDtos = inventoryItemEntities.Select(inventoryItem =>
            {
                var catalogItem = catalogItemEntities.Single(catalogItem => catalogItem.Id == inventoryItem.CatalogItemId);
                return inventoryItem.AsDto(catalogItem.Name, catalogItem.Description);
            });

            return Ok(inventoryItemDtos);
        }
        [HttpPost]
        public async Task<ActionResult> PostAsync(GrantItemDto dto)
        {
            var inventoryItem = await _inventoryItemsRepo.GetAsync(item => item.UserId == dto.UserId && item.CatalogItemId == dto.CatalogItemId);
            if (inventoryItem == null)
            {
                inventoryItem = new InventoryItem
                {
                    CatalogItemId = dto.CatalogItemId,
                    UserId = dto.UserId,
                    Quantity = dto.Quantity,
                    AquiredDate = DateTimeOffset.UtcNow
                };
                await _inventoryItemsRepo.CreateAsync(inventoryItem);
            }
            else
            {
                
                inventoryItem.Quantity = dto.Quantity;
                await _inventoryItemsRepo.UpdateAsync(inventoryItem);
                
            }
            return Ok();
        
        }
    }
}
